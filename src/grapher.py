import matplotlib.pyplot as plt
import config
import OC_kNN

ti_x = []
ti_y = []

def training_waves_graph():
    plt.title('complete training signal')
    plt.plot(config.complete_training_signal)
    plt.show()
    
    
def slope_plot():
    plt.title('slopes of PB high')
    for i in config.slope_set_pb_high:
        plt.plot(i[: -1])
    plt.show()
    plt.title('slopes of PB low')
    for i in config.slope_set_pb_low:
        plt.plot(i[: -1])
    plt.show()


def ti_extractor(s1, s2):
    t1 = []
    t2 = []
    for i in range(len(s1[0])):
        a = []
        b = []
        for j in range(len(s1)):
            a.append(s1[j][i])
            b.append(s2[j][i])
        t1.append(a)
        t2.append(b)
    return t1, t2


def two_input_plot():
    x, y = ti_extractor(OC_kNN.kNN_points_training_set_X['high'], OC_kNN.kNN_points_training_set_Y['high'])
    for i in range(len(x)):
        plt.plot(x[i], y[i])
    plt.show()
    a, b = ti_extractor(OC_kNN.kNN_points_training_set_X['low'], OC_kNN.kNN_points_training_set_Y['low'])
    a.sort(reverse=True)
    for i in range(len(a)):
        plt.plot(a[i], b[i])
    plt.show()