import config
import math
from math import inf


kNN_slope_training_set = {'high': [], 'low': []}
kNN_points_training_set_X = {'high': [], 'low': []}
kNN_points_training_set_Y = {'high': [], 'low': []}
slope_threshold_high = {}
slope_threshold_low = {}
points_threshold_high = {'X': {}, 'Y': {}}
points_threshold_low = {'X': {}, 'Y': {}}

def oc_kNN_dataset():
    for idx, i in enumerate(config.slope_set_pb_high[0][: -1]):
        a = []
        for j in range(len(config.slope_set_pb_high)):
            a.append(config.slope_set_pb_high[j][idx])
        kNN_slope_training_set['high'].append(a)
    for idx, i in enumerate(config.slope_set_pb_low[0][: -1]):
        a = []
        for j in range(len(config.slope_set_pb_low)):
            a.append(config.slope_set_pb_low[j][idx])
        kNN_slope_training_set['low'].append(a)
    for idx, i in enumerate(config.dataset_pb_high[0]):
        a = []
        b = []
        for j in range(len(config.dataset_pb_high)):
            a.append(config.dataset_pb_high[j][idx][0])
            b.append(config.dataset_pb_high[j][idx][1])
        kNN_points_training_set_X['high'].append(a)
        kNN_points_training_set_Y['high'].append((b))
    for idx, i in enumerate(config.dataset_pb_low[0]):
        a = []
        b = []
        for j in range(len(config.dataset_pb_low)):
            a.append(config.dataset_pb_low[j][idx][0])
            b.append(config.dataset_pb_low[j][idx][1])
        kNN_points_training_set_X['low'].append(a)
        kNN_points_training_set_Y['low'].append((b))


def oc_kNN_training():
    for i in range(len(kNN_slope_training_set['high'])):
        slope_threshold_high[i] = 0
        for j in kNN_slope_training_set['high'][i]:
            for k in kNN_slope_training_set['high'][i]:
                if k == j: continue
                else:
                    slope_threshold_high[i] += abs(j - k)
        slope_threshold_high[i] /= len(kNN_slope_training_set['high'][i])
    for i in range(len(kNN_slope_training_set['low'])):
        slope_threshold_low[i] = 0
        for j in kNN_slope_training_set['low'][i]:
            for k in kNN_slope_training_set['low'][i]:
                if k == j: continue
                else:
                    slope_threshold_low[i] += abs(j - k)
        slope_threshold_low[i] /= len(kNN_slope_training_set['low'][i])
    for i in range(len(kNN_slope_training_set['low'])):
        slope_threshold_low[i] = 0
        for j in kNN_slope_training_set['low'][i]:
            for k in kNN_slope_training_set['low'][i]:
                if k == j: continue
                else:
                    slope_threshold_low[i] += abs(j - k)
        slope_threshold_low[i] /= len(kNN_slope_training_set['low'][i])
    for i in range(len(kNN_points_training_set_X)):
        if i == 0:
            for j in range(len(kNN_points_training_set_X['high'])):
                points_threshold_high['X'][j] = 0
                for k in kNN_points_training_set_X['high'][j]:
                    for l in kNN_points_training_set_X['high'][j]:
                        if k == l: continue
                        else:
                            points_threshold_high['X'][j] += abs(k - l)
                mean = points_threshold_high['X'][j] / len(kNN_points_training_set_X['high'][j])
                points_threshold_high['X'][j] = round(mean, 4)
        if i == 1:
            for j in range(len(kNN_points_training_set_X['low'])):
                points_threshold_low['X'][j] = 0
                for k in kNN_points_training_set_X['low'][j]:
                    for l in kNN_points_training_set_X['low'][j]:
                        if k == l: continue
                        else:
                            points_threshold_low['X'][j] += abs(k - l)
                mean = points_threshold_low['X'][j] / len(kNN_points_training_set_X['low'][j])
                points_threshold_low['X'][j] = round(mean, 4)
    for i in range(len(kNN_points_training_set_Y)):
        if i == 0:
            for j in range(len(kNN_points_training_set_Y['high'])):
                points_threshold_high['Y'][j] = 0
                for k in kNN_points_training_set_Y['high'][j]:
                    for l in kNN_points_training_set_Y['high'][j]:
                        if k == l: continue
                        else:
                            points_threshold_high['Y'][j] += abs(k - l)
                points_threshold_high['Y'][j] /= len(kNN_points_training_set_Y['high'][j])
        if i == 1:
            for j in range(len(kNN_points_training_set_Y['low'])):
                points_threshold_low['Y'][j] = 0
                for k in kNN_points_training_set_Y['low'][j]:
                    for l in kNN_points_training_set_Y['low'][j]:
                        if k == l: continue
                        else:
                            points_threshold_low['Y'][j] += abs(k - l)
                points_threshold_low['Y'][j] /= len(kNN_points_training_set_Y['low'][j])


def closest(lst, K):
    close = []
    for idx, i in enumerate(lst):
        if i == K:
            close.append(math.inf)
        else:
            close.append(abs(K - i))
    return lst[close.index(min(close))]



def distance_ratio(z, y, x):
    dzy = abs(z - y)
    dyx = abs(y - x)
    return round((dzy / dyx), 4)


def classify(value, delta):
    if value < delta:
        return 'Default Class'
    else:
        return 'Outlier'


def oc_kNN_testing_points(points):
    flag = False
    valChanged = False
    dzy_x = []
    dzy_y = []
    t = 0
    t2 = math.inf
    prev_t2 = t2
    der_dist_x = []
    der_dist_y = []
    ratio_list_x = []
    ratio_list_y = []
    class_list_x = []
    class_list_y = []
    ind_x = 0
    ind_y = 0
    for idx, i in enumerate(points):
        dist_x = 0
        dist_y = 0
        if i[0] < 0:
            for j in range(len(kNN_points_training_set_X['low'][idx])):
                t = abs(i[0] - j)
                t2 = min(t2, t)
                valChanged= t2 != prev_t2
                if valChanged:
                    prev_t2 = t2
                    dist_x = kNN_points_training_set_X['low'][idx][j]
                    ind_x = idx
                else:
                    prev_t2 = t2
                    continue
            t = 0
            t2 = math.inf
            prev_t2 = t2
            valChanged = False
            for j in range(len(kNN_points_training_set_Y['low'][idx])):
                t = abs(i[0] - j)
                t2 = min(t2, t)
                valChanged = t2 != prev_t2
                if valChanged:
                    prev_t2 = t2
                    dist_y = kNN_points_training_set_Y['low'][idx][j]
                    ind_y = idx
                else:
                    prev_t2 = t2
                    continue
            dzy_x.append(dist_x)
            der_dist_x.append(closest(kNN_points_training_set_X['low'][ind_x], dist_x))
            tmp = distance_ratio(i[0], dist_x, closest(kNN_points_training_set_X['low'][ind_x], dist_x))
            ratio_list_x.append(tmp)
            class_list_x.append(classify(tmp, 1))
            dzy_y.append(dist_y)
            der_dist_y.append(closest(kNN_points_training_set_Y['low'][ind_y], dist_y))
            tmp2 = distance_ratio(i[1], dist_y, closest(kNN_points_training_set_Y['low'][ind_y], dist_y))
            ratio_list_y.append(tmp2)
            class_list_y.append(classify(tmp2, 1))

        if i[0] > 0:
            for j in range(len(kNN_points_training_set_X['high'][idx])):
                t = abs(i[0] - j)
                t2 = min(t2, t)
                valChanged = t2 != prev_t2
                if valChanged:
                    prev_t2 = t2
                    dist_x = kNN_points_training_set_X['high'][idx][j]
                    ind_x = idx
                else:
                    prev_t2 = t2
                    continue
            t = 0
            t2 = math.inf
            prev_t2 = t2
            valChanged = False
            for j in range(len(kNN_points_training_set_Y['high'][idx])):
                t = abs(i[0] - j)
                t2 = min(t2, t)
                valChanged = t2 != prev_t2
                if valChanged:
                    prev_t2 = t2
                    dist_y = kNN_points_training_set_Y['high'][idx][j]
                    ind_y = idx
                else:
                    prev_t2 = t2
                    continue
            dzy_x.append(dist_x)
            der_dist_x.append(closest(kNN_points_training_set_X['high'][ind_x], dist_x))
            tmp = distance_ratio(i[0], dist_x, closest(kNN_points_training_set_X['high'][ind_x], dist_x))
            ratio_list_x.append(tmp)
            class_list_x.append(classify(tmp, 1))
            dzy_y.append(dist_y)
            der_dist_y.append(closest(kNN_points_training_set_Y['high'][ind_y], dist_y))
            tmp2 = distance_ratio(i[1], dist_y, closest(kNN_points_training_set_Y['high'][ind_y], dist_y))
            ratio_list_y.append(tmp2)
            class_list_y.append(classify(tmp2, 1))
    return dzy_x, dzy_y, der_dist_x, der_dist_y, ratio_list_x, ratio_list_y, class_list_x, class_list_y






# def oc_kNN():
    