import numpy as np



def normalize_score(warping_cost, x_max, x_length):
    if x_max == 0:
        x_max_t = 0.00000001
    else:
        x_max_t = x_max
    temp_max = x_length * x_max_t
    norm_score = (temp_max - warping_cost) / temp_max
    return norm_score


def DDTW_SakoeChiba(X, Y, signal_max_value):
    m = len(X)
    n = len(Y)
    # print('\nDistance of Derivatives:')
    w = max(5, abs(m - n))
    path = []
    path_elements = []
    r = 0
    s = 0
    metric = 0
    ddtw = np.matrix(np.ones((n, m)) * np.inf)
    ddtw[0, 0] = 0
    for i in range(1, n):
        for j in range(max(1, i - w), min(m, i + w)):
            ddtw[i, j] = 0
    for i in range(1, n):
        for j in range(max(1, i - w), min(m, i + w)):
            # if (i == 0 and j == 0) or (i == 0 and j != 0) or (j == 0 and i != 0):
            #     continue
            if i + 1 >= n or j + 1 >= m:
                continue
            # elif ddtw[i, j + 1] == np.inf or ddtw[i, j - 1] == np.inf or ddtw[i + 1, j] == np.inf or ddtw[i - 1, j] == np.inf:
            #     continue
            else:
                if ddtw[i, j] == np.inf:
                    continue
                elif ddtw[i, j + 1] == np.inf or ddtw[i + 1, j] == np.inf:
                    derDist = float((X[j] - Y[i]) ** 2)
                else:
                    dX = ((X[j] - X[j - 1]) + ((X[j + 1] - X[j - 1]) / 2.0)) / 2.0
                    dY = ((Y[i] - Y[i - 1]) + ((Y[i + 1] - Y[i - 1]) / 2.0)) / 2.0
                    derDist = float((dX - dY) ** 2)
                ddtw[i, j] = derDist + min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1])
                if ddtw[i - 1, j] == min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1]):
                    r = i - 1
                elif ddtw[i, j - 1] == min(min(ddtw[i - 1, j], ddtw[i - 1, j - 1]), ddtw[i, j - 1]):
                    s = j - 1
                else:
                    r = i - 1
                    s = j - 1
        # path.append([s, r])
        # path_elements.append(ddtw[r, s])
        # metric += ddtw[r, s]
        if ddtw[r, s] == np.inf:
            print(i, '\t', metric)
        else:
            path.append([s, r])
            path_elements.append(ddtw[r, s])
            metric += ddtw[r, s]
    WarpingCost = (metric ** 0.5)
    normalized_cost = normalize_score(WarpingCost, signal_max_value, m)
    return normalized_cost
    # return WarpingCost