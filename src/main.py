import config
import to_feature_space
import signal_input
import grapher
import matplotlib.pyplot as plt
import OC_kNN
import correlation
import dtw
import OC_svm

signal_input.train_data()
print()
print('***********************************************************')
print('PB dataset')
print('High part - two inputs')
print(*config.dataset_pb_high, sep='\n')
print()
print('Low part - two inputs')
print(*config.dataset_pb_low, sep='\n')
print()
print('High part - slopes')
print(*config.slope_set_pb_high, sep='\n')
print()
print('Low part - slopes')
print(*config.slope_set_pb_low, sep='\n')
print('***********************************************************')
print()
print('***********************************************************')
print('Up dataset')
print('High part - two inputs')
print(*config.dataset_up_high, sep='\n')
print()
print('Low part - two inputs')
print(*config.dataset_up_low, sep='\n')
print()
print('High part - slopes')
print(*config.slope_set_up_high, sep='\n')
print()
print('Low part - slopes')
print(*config.slope_set_up_low, sep='\n')
print('***********************************************************')
grapher.training_waves_graph()

print()
# signal_input.test_data()
signal_input.test_data_continuous()
print('***********************************************************')
print('two input test set - High')
print(*config.testing_dataset_high, sep='\n')
print()
print('two input test set - Low')
print(*config.testing_dataset_low, sep='\n')
print()
print('slopes test set - High')
print(*config.testing_slope_set_high, sep='\n')
print()
print('slopes test set - Low')
print(*config.testing_slope_set_low, sep='\n')
print('***********************************************************')

# plt.plot(config.complete_testing_signal)
# plt.show()
grapher.slope_plot()
OC_kNN.oc_kNN_dataset()
OC_kNN.oc_kNN_training()
print()
print('***********************************************************')
print('slope dataset - high')
print(*OC_kNN.kNN_slope_training_set['high'], sep='\n')
print('slope dataset - low')
print(*OC_kNN.kNN_slope_training_set['low'], sep='\n')
print()
print('points dataset - high')
print(*OC_kNN.kNN_points_training_set_X['high'], sep='\n')
print(*OC_kNN.kNN_points_training_set_Y['high'], sep='\n')
print('points dataset - low')
print(*OC_kNN.kNN_points_training_set_X['low'], sep='\n')
print(*OC_kNN.kNN_points_training_set_Y['low'], sep='\n')
print('***********************************************************')
print()
print('***********************************************************')
print('slope threshold high')
print(*OC_kNN.slope_threshold_high.items(), sep='\n')
print('slope threshold low')
print(*OC_kNN.slope_threshold_low.items(), sep='\n')
print()
print('points threshold high')
print(*OC_kNN.points_threshold_high.items(), sep='\n')
print('points threshold low')
print(*OC_kNN.points_threshold_low.items(), sep='\n')
print('***********************************************************')

# testing_data = [[i[0], i[1]] for i in config.testing_dataset_high[0]]
# print()
# print('test data')
# print(testing_data)
# closest_neighbor_x, closest_neighbor_y, closest_element_to_neighbor_x, closest_element_to_neighbor_y, dist_ratio_x, dist_ratio_y, class_x, class_y = OC_kNN.oc_kNN_testing_points(testing_data)
# print()
# print('closest x neighbors:', closest_neighbor_x)
# print('closest y neighbors:', closest_neighbor_y)
# print('closest element to x neighbor:', closest_element_to_neighbor_x)
# print('closest element to y neighbor:', closest_element_to_neighbor_y)
# print('distance ratios of x', dist_ratio_x)
# print('distance ratios of y', dist_ratio_y)
# print('>> class of x:', class_x)
# print('>> class of y:', class_y)

print()
print('***********************************************************')
high_slope_tmp = [[j for j in i if j != 1] for i in config.slope_set_pb_high]
low_slope_tmp = [[j for j in i if j != 1] for i in config.slope_set_pb_low]
# print('high temp slopeset')
# print(*high_slope_tmp, sep='\n')
high_slope_mean = to_feature_space.mean_slope(high_slope_tmp)
low_slope_mean = to_feature_space.mean_slope(low_slope_tmp)
high_testing_slopes = [[j for j in i if j != 1] for i in config.testing_slope_set_high]
low_testing_slopes = [[j for j in i if j != 1] for i in config.testing_slope_set_low]
high_slope_corr = []
low_slope_corr = []
high_slope_corr = [[correlation.pearson_corr(high_slope_mean, i)] for i in high_testing_slopes]
low_slope_corr = [[correlation.pearson_corr(low_slope_mean, i)] for i in low_testing_slopes]
print('correlation of high testing data with mean of training slopes')
print(*high_slope_corr, sep='\n')
print('correlation of low testing data with mean of training slopes')
print(*low_slope_corr, sep='\n')

grapher.two_input_plot()

plt.title('slope test data - high')
for i in high_testing_slopes:
    plt.plot(i)
plt.show()
plt.title('slope test data- low')
for i in low_testing_slopes:
    plt.plot(i)
plt.show()

# slope_dtw_costs_high = [dtw.DDTW_SakoeChiba(high_slope_mean, high_testing_slopes[i], max(max(high_slope_mean), max(high_testing_slopes[i]))) for i in range(len(high_testing_slopes))]
# slope_dtw_costs_low = [dtw.DDTW_SakoeChiba(low_slope_mean, low_testing_slopes[i], max(max(low_slope_mean), max(low_testing_slopes[i]))) for i in range(len(high_testing_slopes))]
# print()
# print('DTW costs for high testing data (PB)')
# print(*slope_dtw_costs_high, sep='\n')
# print('DTW costs for low testing data (PB)')
# print(*slope_dtw_costs_low, sep='\n')


OC_svm.get_training_set()
OC_svm.get_testing_set()
print()
print('***********************************************************')
OC_svm.display_datasets()
OC_svm.train_classifier()
OC_svm.test_model()
print()
OC_svm.display_outputs()


