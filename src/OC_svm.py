import numpy as np
from sklearn import svm
import config

svm_pb_high_points_trainset = {1:[], 2:[], 3:[], 4:[]}
svm_pb_low_points_trainset = {1:[], 2:[], 3:[], 4:[]}

svm_high_points_testset = {1:[], 2:[], 3:[], 4:[]}
svm_low_points_testset = {1:[], 2:[], 3:[], 4:[]}

clf_high = {}
clf_low = {}
pred_high = {1:[], 2:[], 3:[], 4:[]}
pred_low = {1:[], 2:[], 3:[], 4:[]}

def get_training_set():
    global svm_pb_high_points_trainset, svm_pb_low_points_trainset
    svm_tmp = [*svm_pb_high_points_trainset]
    for idx, i in enumerate(svm_tmp):
        for j in range(len(config.dataset_pb_high)):
            svm_pb_high_points_trainset[i].append(config.dataset_pb_high[j][i - 1])
        for j in range(len(config.dataset_pb_low)):
            svm_pb_low_points_trainset[i].append(config.dataset_pb_low[j][i - 1])

def get_testing_set():
    global svm_high_points_testset, svm_low_points_testset
    svm_tmp = [*svm_high_points_testset]
    for idx, i in enumerate(svm_tmp):
        for j in range(len(config.testing_dataset_high)):
            svm_high_points_testset[i].append(config.testing_dataset_high[j][i - 1])
        for j in range(len(config.testing_dataset_low)):
            svm_low_points_testset[i].append(config.testing_dataset_low[j][i - 1])

def display_datasets():
    global svm_pb_high_points_trainset, svm_pb_low_points_trainset
    global svm_high_points_testset, svm_low_points_testset
    print('PB high points set for training')
    for i, j in svm_pb_high_points_trainset.items():
        print(i, '\t', j)
    print('PB low points set for training')
    for i, j in svm_pb_low_points_trainset.items():
        print(i, '\t', j)
    print()
    print('high testing points dataset')
    for i, j in svm_high_points_testset.items():
        print(i, '\t', j)
    print('low testing points dataset')
    for i, j in svm_low_points_testset.items():
        print(i, '\t', j)


def train_classifier():
    global clf_high, clf_low, svm_pb_high_points_trainset, svm_pb_low_points_trainset
    svm_tmp = [*svm_pb_high_points_trainset]
    for idx, i in enumerate(svm_tmp):
        clf_high[i] = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
        clf_low[i] = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
        clf_high[i].fit(svm_pb_high_points_trainset[i])
        clf_low[i].fit(svm_pb_low_points_trainset[i])

def test_model():
    global clf_high, clf_low, svm_pb_high_points_trainset, svm_pb_low_points_trainset
    global svm_high_points_testset, svm_low_points_testset
    global pred_high, pred_low
    svm_tmp = [*svm_high_points_testset]
    for idx, i in enumerate(svm_tmp):
        pred_high[i].append(clf_high[i].predict(svm_high_points_testset[i]))
        pred_low[i].append(clf_high[i].predict(svm_low_points_testset[i]))
        # for j in range(len(svm_high_points_testset[i])):
        #     test_data = np.array(svm_high_points_testset[i][j]).reshape(1, -1)
        #     pred_high[i].append(clf_high[i].predict(test_data))
        # for j in range(len(svm_low_points_testset[i])):
        #     test_data = np.array(svm_low_points_testset[i][j]).reshape(1, -1)
        #     pred_low[i].append(clf_high[i].predict(test_data))


def display_outputs():
    global pred_high, pred_low
    print('test data predictions - high')
    for i, j in pred_high.items():

        print(i, '\t', j)
    print('test data predictions - low')
    for i, j in pred_low.items():
        print(i, '\t', j)