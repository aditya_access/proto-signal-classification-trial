from scipy.stats import pearsonr
import statistics


def pearson_corr(compare_with, to_compare):
    corr, _ = pearsonr(compare_with, to_compare)
    return corr

