

# container objects for training waves
complete_training_signal = []

training_pb = []
training_up = []

training_pb_high = []
training_pb_low = []

training_up_high = []
training_up_low = []
###

# container objects for feature space inputs
feature_dict_pb_high = {}
feature_dict_up_high = {}

feature_dict_pb_low = {}
feature_dict_up_low = {}

dataset_pb_high = []
dataset_pb_low = []

dataset_up_high = []
dataset_up_low = []

slope_set_pb_high = []
slope_set_pb_low = []

slope_set_up_high = []
slope_set_up_low = []
###

# container objects for testing waves
complete_testing_signal = []

testing_high = []
testing_low = []

testing_dataset_high = []
testing_dataset_low = []

testing_slope_set_high = []
testing_slope_set_low = []
###

# Neural Network related containers
layers_neuron_outputs = []

network_weights = {}
###