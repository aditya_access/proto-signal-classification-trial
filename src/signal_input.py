import pandas as pd
import tkinter as tk
from tkinter import filedialog
import config
import glob
import cleanup
import to_feature_space
import matplotlib.pyplot as plt

testPath_pb = r'C:\Users\Aditya\EOG Project\TEST_03\CSV READINGS\PB_test'
testPath_up = r'C:\Users\Aditya\EOG Project\TEST_03\CSV READINGS\U_test'
testFiles_pb = glob.glob(testPath_pb + "/*.csv")
testFiles_up = glob.glob(testPath_up + "/*.csv")

train_names = ['train_Pbs', 'train_Us']
test_names = ['test_PB', 'test_U']

def train_data():
    global train_names
    n_sub_divide = 4
    for i in train_names:
        print('enter training data')
        csv_path = filedialog.askopenfilename()
        readCSV = pd.read_csv(csv_path)
        for j in readCSV[i].tolist():
            sample = round(cleanup.clean_and_get_FOD(j), 2)
            config.complete_training_signal.append(sample)
            to_feature_space.fit_into_ranges(sample, n_sub_divide, i)
    plt.show()

def test_data():
    global test_names, testPath_pb, testPath_up, testFiles_pb, testFiles_up
    n_sub_divide = 4
    for i in test_names:
        for filename in testFiles_pb:
            readCSV = pd.read_csv(filename)
            for j in readCSV['test_PB'].tolist():
                sample = cleanup.clean_and_get_FOD(j)
                to_feature_space.fit_into_ranges(sample, n_sub_divide, i)
        for filename in testFiles_up:
            readCSV = pd.read_csv(filename)
            for j in readCSV['test_U'].tolist():
                sample = cleanup.clean_and_get_FOD(j)
                to_feature_space.fit_into_ranges(sample, n_sub_divide, i)

def test_data_continuous():
    global test_names
    n_sub_divide = 4
    for i in test_names:
        print('enter testing data')
        csv_path = filedialog.askopenfilename()
        readCSV = pd.read_csv(csv_path)
        for j in readCSV[i].tolist():
            sample = round(cleanup.clean_and_get_FOD(j), 2)
            config.complete_testing_signal.append(sample)
            to_feature_space.fit_into_ranges(sample, n_sub_divide, i)