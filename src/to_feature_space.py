import config


_min = 0
_max = 0
down = []
up = []
sent_flag_d = False
sent_flag_u = False
high_dataset = {}
low_dataset = {}
prev_elem = 0
extrema_count = 0
high_threshold = 0.04
low_threshold = -0.04
non_classed_slopes = []


def acquire_ranges(wavelet, extrema_index, number_of_subdivides):
    range_dict = {}
    if wavelet[0] > 0:
        d = round(((max(wavelet) - min(wavelet)) / number_of_subdivides), 3)
        a = 0
        c = 0
        for i in range(number_of_subdivides):
            a += d
            c = min(wavelet) + a
            range_dict[round(c, 3)] = 0
        range_list = [*range_dict]
        for idx, i in enumerate(wavelet):
            for k, l in range_dict.items():
                if (k - d) <= i <= k:
                    for m in range_list[: range_list.index(k) + 1]:
                        range_dict[m] += 1
                    continue
    if wavelet[0] < 0:
        d = round(((min(wavelet) - max(wavelet)) / number_of_subdivides), 5)
        a = 0
        c = 0
        for i in range(number_of_subdivides):
            a += d
            c = max(wavelet) + a
            range_dict[round(c, 3)] = 0
        range_list = [*range_dict]
        for idx, i in enumerate(wavelet):
            for k, l in range_dict.items():
                if abs(k - d) <= abs(i) <= abs(k):
                    for m in range_list[: range_list.index(k) + 1]:
                        range_dict[m] += 1
                    continue
    return range_dict


def points_of_feature_wave(dataset_dict, train_type):
    set = [[i, j] for i, j in dataset_dict.items()]
    return set


def slopes_of_feature_wave(dataset_dict, train_type):
    global non_classed_slopes
    y = [j for i, j in dataset_dict.items()]
    x = [i for i, j in dataset_dict.items()]
    slopes = []
    for i in range(len(x)):
        if i == 0:
            continue
        else:
            a = y[i]
            b = y[i - 1]
            c = x[i]
            d = x[i - 1]
            slopes.append(round(((a - b) / (c - d)), 3))
    non_classed_slopes = slopes
    # slopes.append(train_type)
    return slopes



def fit_into_ranges(datapoint, number_of_subdivides, train_type_str):
    global _min, _max, up, down, sent_flag_u, sent_flag_d, high_dataset, low_dataset, prev_elem, extrema_count, high_threshold, low_threshold
    train_type = -2
    if train_type_str in ['train_Pbs', 'test_PB']: train_type = 1
    if train_type_str in ['train_Us', 'test_U']: train_type = 0
    if low_threshold <= datapoint <= high_threshold:
        prev_elem = 0
        _min = 0
        _max = 0
        sent_flag_u = False
        sent_flag_d = False
        down = []
        up = []
        extrema_count = 0
    if datapoint < low_threshold:
        _min = min(_min, datapoint)
        down.append(datapoint)
        if _min < prev_elem and not sent_flag_d:
            extrema_count += 1
            if extrema_count == 20:
                low_dataset = acquire_ranges(down[: down.index(_min) + 1], down.index(_min), number_of_subdivides)
                sent_flag_d = True
                extrema_count = 0
                prev_elem = 0
                if train_type == 1 and not train_type_str in ['test_PB', 'test_U']:
                    config.dataset_pb_low.append(points_of_feature_wave(low_dataset, train_type))
                    config.slope_set_pb_low.append(slopes_of_feature_wave(low_dataset, train_type))
                elif train_type == 0 and not train_type_str in ['test_PB', 'test_U']:
                    config.dataset_up_low.append(points_of_feature_wave(low_dataset, train_type))
                    config.slope_set_up_low.append(slopes_of_feature_wave(low_dataset, train_type))
                if train_type_str in ['test_PB', 'test_U']:
                    config.testing_dataset_low.append(points_of_feature_wave(low_dataset, train_type))
                    config.testing_slope_set_low.append(slopes_of_feature_wave(low_dataset, train_type))
        prev_elem = datapoint
    if datapoint > high_threshold:
        _max = max(_max, datapoint)
        up.append(datapoint)
        if _max > prev_elem and not sent_flag_u:
            extrema_count += 1
            if extrema_count == 20:
                high_dataset = acquire_ranges(up[: up.index(_max) + 1], up.index(_max), number_of_subdivides)
                sent_flag_u = True
                extrema_count = 0
                prev_elem = 0
                if train_type == 1 and not train_type_str in ['test_PB', 'test_U']:
                    config.dataset_pb_high.append(points_of_feature_wave(high_dataset, train_type))
                    config.slope_set_pb_high.append(slopes_of_feature_wave(high_dataset, train_type))
                elif train_type == 0 and not train_type_str in ['test_PB', 'test_U']:
                    config.dataset_up_high.append(points_of_feature_wave(high_dataset, train_type))
                    config.slope_set_up_high.append(slopes_of_feature_wave(high_dataset, train_type))
                if train_type_str in ['test_PB', 'test_U']:
                    config.testing_dataset_high.append(points_of_feature_wave(high_dataset, train_type))
                    config.testing_slope_set_high.append(slopes_of_feature_wave(high_dataset, train_type))
        prev_elem = datapoint



def mean_slope(all_slopes):
    mean = []
    for idx, i in enumerate(all_slopes):
        if idx == 0:
            for j in i:
                mean.append(j)
        else:
           for idx2, j in enumerate(i):
                mean[idx2] += j
    for i in range(len(mean)):
        mean[i] /= len(all_slopes)
    return mean